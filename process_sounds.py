import subprocess
import os

blender = '/Applications/blender.app/Contents/MacOS/blender'

def isWave(filename):
  return filename.find('.wav') != -1

wave_files = filter(isWave, os.listdir('.'))
for wave in wave_files:
  output_filename = wave[:-4]+'_output'
  subprocess.call("pwd")
  subprocess.call("python2 sound_analyze.py {0}".format(wave), shell=True)
  subprocess.call("{0} main.blend --python generate_cube.py --background -o //{1} --render-frame 0".format(blender,output_filename ), shell=True)
  subprocess.call("{0} main.blend --python generate_cube.py --background -o //{1} --render-anim".format(blender,output_filename+'_anim' ), shell=True)