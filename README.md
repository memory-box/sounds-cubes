I added Blender as an alias:

`echo "alias blender=/Applications/blender.app/Contents/MacOS/blender" >> ~/.bash_profile`

Audio analysis: 

`python2 sound_analyze.py $filename.wav`

To run the script: 

`blender main.blend --python generate_cube.py`

(The `.blend` file argument needs to come first otherwise it won't load)

To render immediately: 

`blender main.blend --python generate_cube.py --background -o // --render-frame 0`

reqs: 

`brew install python2 && brew install python3 && brew install libmagic`
`pip2 install pyAudioAnalysis`


I wrote several Python scripts that work as follows:

1. A sound analyzer that accepts a mono .wav file and analyses it. It outputs the median energy levels of the file divided to 8 equal chunks, and the sample length. Those values are then saved to a CSV file.
2. A Blender script that should be executed on a mostly empty .blend file I created. It loads the CSV, and generates an object in the file.
3. A script that batch run the previous two scripts on all .wav files in a directory, and runs blender in the background to render AVI and PNG files displaying the objects.

If I were to make a server that accepts .wav files and outputs .obj files, these are the next steps required:

1. Set up a linux server with Blender and the required Python libraries installed.
2. Set up a web server (Flask perhaps) that accepts POST requests with wav files and makes sure that they're in the right format (mono wav)
3. Alter the Blender script (#2) in such a way that it also exports an OBJ file to some local storage
4. Have the web server run the script and return the OBJ file as a downloadable