from pyAudioAnalysis import audioBasicIO
from pyAudioAnalysis import audioFeatureExtraction
import math
import csv
import sys

def median(lst):
  n = len(lst)
  if n < 1:
    return None
  if n % 2 == 1:
    return sorted(lst)[n//2]
  else:
    return sum(sorted(lst)[n//2-1:n//2+1])/2.0

[Fs, x] = audioBasicIO.readAudioFile(sys.argv[1]);
F = audioFeatureExtraction.stFeatureExtraction(x, Fs, 0.050*Fs, 0.025*Fs);
energy_graph = F[1,:]
sample_length = len(energy_graph)
chunk_size = int(math.floor(sample_length/8))
max_energy = round(max(energy_graph),8)
min_energy = round(min(energy_graph),8)
print (sample_length, chunk_size, max_energy, min_energy) 
averages = []
medians = []
for chunk in range(0,8):
  average_of_chunk = sum(energy_graph[chunk*chunk_size:chunk*chunk_size+chunk_size])/chunk_size
  average_of_chunk = average_of_chunk / (max_energy-min_energy)+min_energy
  averages.append(average_of_chunk)
  median_of_chunk = median(energy_graph[chunk*chunk_size:chunk*chunk_size+chunk_size])
  median_of_chunk = median_of_chunk / (max_energy-min_energy)+min_energy
  medians.append(median_of_chunk)

with open('input.csv', 'wb') as csvfile:
  writer = csv.writer(csvfile)
  writer.writerow(averages)
  writer.writerow([sample_length])
  writer.writerow(sys.argv[1])

print(averages)
print(medians)