
gcloud compute --project "sounds-cubes" ssh --zone "europe-west1-b" "instance-1"

sudo apt-get update
sudo apt-get install python-setuptools python-dev build-essential
sudo easy_install pip

sudo add-apt-repository ppa:thomas-schiex/blender
sudo apt-get install blender

sudo pip install libmagic
sudo pip install pyAudioAnalysis
sudo apt-get install python-tk

gcloud compute scp ./* \instance-1:~/app 

python sound_analyze.py filename.wav
blender main.blend --python generate_cube.py --background -o // --render-frame 0

gcloud compute scp instance-1:~/app/*.png .
