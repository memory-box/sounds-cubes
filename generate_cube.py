import bpy
import math
import random
import csv
import sys
import os

cube_values = []

def translate(value, leftMin, leftMax, rightMin, rightMax):
    leftSpan = leftMax - leftMin
    rightSpan = rightMax - rightMin
    valueScaled = float(value - leftMin) / float(leftSpan)
    return rightMin + (valueScaled * rightSpan)

filename = ''
with open('input.csv', newline='') as csvfile:
    csvfile = csv.reader(csvfile)
    lines = []
    for line in csvfile:
        lines.append(line)
    for number in lines[0]:
        cube_values.append(float(number))
    cube_values.append(float(lines[1][0]))
    filename = ''.join(lines[2])

for mesh in bpy.data.meshes:
    bpy.data.meshes.remove(mesh)

bpy.ops.mesh.primitive_cube_add(location=(0,0,0), radius=2)
main_cube = bpy.context.active_object
mat = bpy.data.materials.get("Material")
main_cube.data.materials.append(mat)

bpy.context.scene.objects.active = bpy.data.objects['Cube']

cube_size = 3
cube_number=0
for level in [-1,1]:
    for corner in [0,1,2,3]:
        rotation = [0,0,0]
        rotation[1] = math.pi * 0.25 * level
        rotation[2] = math.pi * 0.25 + math.pi * 0.5 * corner + cube_values[cube_number]*15
        bpy.ops.mesh.primitive_cube_add(
            location = (0,0,0),
            rotation = tuple(rotation),
            radius = cube_size
        )
        bool = main_cube.modifiers.new("Boolean", 'BOOLEAN')
        bool.object = bpy.context.active_object
        remove_later = bpy.context.active_object
        offset = cube_values[cube_number]*(6.8-4)+4
        # min: 4 max: 6.8
        for vertex in bpy.context.active_object.data.vertices:
            vertex.co.x += offset
        bool.operation = 'DIFFERENCE'
        bpy.context.scene.objects.active = main_cube
        bpy.ops.object.modifier_apply(apply_as='DATA', modifier="Boolean")
        bpy.data.objects.remove(remove_later)
        cube_number += 1

scale_factor = translate(cube_values[8],100,6000,0.5,6)
scale_factor = min(scale_factor,6)
main_cube.scale = [1.0,1.0, scale_factor]

blend_file_path = bpy.data.filepath
directory = os.path.dirname(blend_file_path)
target_file = os.path.join(directory, filename+'.obj')

bpy.ops.export_scene.obj(filepath=target_file)

scene = bpy.data.scenes["Scene"]
scene.frame_start = 1
scene.frame_end = 40

main_cube.rotation_euler = (0, 0, 0)
main_cube.keyframe_insert('rotation_euler', index=2 ,frame=1)

main_cube.rotation_euler = (0, 0, math.radians(180))
main_cube.keyframe_insert('rotation_euler', index=2 ,frame=40)

scene.render.use_stamp = 0
if '--render-anim' in sys.argv:
    scene.render.image_settings.file_format = "AVI_JPEG"
